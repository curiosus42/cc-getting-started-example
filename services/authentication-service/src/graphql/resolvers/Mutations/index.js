import login from './login';
import signup from './signup';

const mutationResolvers = {
    ...login,
    ...signup
}

export default mutationResolvers;