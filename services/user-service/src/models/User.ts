import mongoose, { Schema, Document } from 'mongoose';

export interface IUser {
    username: string;
    id: string;
}

interface IUserModel extends Document, IUser {

}

const UserSchema:Schema<IUser> = new Schema({
    id: {
        type: String,
        required: true,
        unique: true,
        maxlength: 36
    },
    username: {
        type: String,
        required: true
    },
});


const User = mongoose.model<IUserModel>('User', UserSchema); 

export default User;