import { Application } from 'express';

import updateUsername from './updateUsername';
import check from './check';

export default (router) => {
    updateUsername(router);
    check(router);
}