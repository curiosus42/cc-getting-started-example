import Leaderboard from '../../../core/Leaderboard';
import { Context } from '../../../start-service';
import LeaderboardMapper from '../../../data-mappers/LeaderboardMapper';

const leaderboardResolver  = async (parent: void, args:void, context:Context, info:any): Promise<Leaderboard> => {
    let lbMapper = new LeaderboardMapper(context.pool);
    let leaderBoard = await lbMapper.get();

    return leaderBoard;
}



export default leaderboardResolver;


