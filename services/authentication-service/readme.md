# authentication-service
The only purpose of this service is to create and sign JSON Web Tokens (JWTs) which are sent to the client and consumed by all services. Therefore it needs to store the username and teh hashed password.

## Key features
* creates signed JWTs
* stores username and hashed password

## Authentication mechanism

The mechanism works as follow:

![Authentication flow](authentication-flow.jpg)
*Authentication flow*

The client calls the authentication-service either with the `login` or `signup` mutation. The service checks the provided password and username or creates a new user and hashes its password. In both cases a JWT is created with the provided username and signed with our signature `f1BtnWgLMNpO`, which is set in the `docker-compose` files.

These JWTs can be validated by any service which has access to the signature. The tokens are valid for one day.

Read more about JWT here: https://jwt.io/introduction/


## Available test users

    {
        username: "Benedikt Gack",
        password: "secret",
        id: "c7025528-f47d-43a5-bcfe-e972835fb417", //fixed ids for reference purposes
    },
    {
        username: "Yannick Möller",
        password: "secret",
        id: "540c2778-60c7-4911-9238-1460710018a0",
    },
    {
        username: "Janina Mattes",
        password: "secret",
        id: "640c2778-60c7-4911-9238-3333330018a0",
    }