import queriesResolvers from './Query';
import mutationsResolvers from './Mutations';

const resolvers = {
  Query: {
    ...queriesResolvers
  },
  Mutation: {
    ...mutationsResolvers
  }
}

export default resolvers;