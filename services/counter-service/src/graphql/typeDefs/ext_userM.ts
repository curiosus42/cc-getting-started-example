import { gql } from 'apollo-server';

const mTypeDefs = gql`
    type Mutation {
        """
        Increments the counter of the current by 1 and returns the updated counter.
        """
        clickOnce: Int
    }
`;

export default mTypeDefs;