import login from './login';
import signup from './signup';

import { gql } from 'apollo-server';

const typeDefs = gql`
    ${login}
    ${signup}
`

export default typeDefs;