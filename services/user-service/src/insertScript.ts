import User from './models/User';

export default async function insertScript() {

    await User.deleteMany({}).exec();

    await User.insertMany([{
        id: "c7025528-f47d-43a5-bcfe-e972835fb417", //fixed ids for reference purposes
        username: "Benedikt Gack"
    },
    {
        id: "540c2778-60c7-4911-9238-1460710018a0",
        username: "Yannick Möller"
    },
    {
        id: "640c2778-60c7-4911-9238-3333330018a0",
        username: "Janina Mattes"
    }]);

    console.log("INSERTED DATA!");
}
