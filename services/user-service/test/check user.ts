import {strict as assert} from 'assert'
import mongoose from 'mongoose';

import DatabaseManager from './DatabaseManager';

import meResolver from '../src/graphql/resolvers/Query/me';
import User from '../src/models/User';

import {Context} from '../src/start-service';


describe("# Check username resolver", () => {
    before("Create db conenction", async () => {
        DatabaseManager.connected() ? true : await DatabaseManager.openConnection();
        await DatabaseManager.resetDb();
        await DatabaseManager.loadInsertScript();
        return
    })
    it("returns the correct username", async () => {
        const userContext:Context = {
            user: {
                id: "c7025528-f47d-43a5-bcfe-e972835fb417",
                username: "Benedikt Gack"
            }
        }
        const result = await meResolver(null, null, userContext);
        console.log(result);
        assert.strictEqual(result.username, "Benedikt Gack");
    })
})