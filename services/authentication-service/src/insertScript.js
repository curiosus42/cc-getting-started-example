import User from "#root/models/User";

const testUserData = [
  {
    username: "Benedikt Gack",
    password: "secret",
    id: "c7025528-f47d-43a5-bcfe-e972835fb417", //fixed ids for reference purposes
  },
  {
    username: "Yannick Möller",
    password: "secret",
    id: "540c2778-60c7-4911-9238-1460710018a0",
  },
  {
    username: "Janina Mattes",
    password: "secret",
    id: "640c2778-60c7-4911-9238-3333330018a0",
  },
];

const run = async () => {
  try {
    await User.deleteMany({});

    //don't use this. It won't trigger the pre hooks from mongoose
    //await User.insertMany(testUserData);

    testUserData.forEach(async data => {
      console.log("usr", data)
      let usr = new User(data);
      await usr.save();
    })

    console.log(`Successfully inserted ${testUserData.length} test users.`)


  } catch (err) {
    console.error("Couldn't insert Testdata:", err);
  }
};

export default run;
