import { Pool } from 'pg';

class DatabaseManager {

    private static retryCount = 5;

    private static isConnected:boolean = false;

    public static pool;

    private static async connect(counter:number):Promise<Pool> {
        if (counter <= DatabaseManager.retryCount) {
            let pool = new Pool();
            try {
                await pool.query("SELECT NOW()");
            } catch (err) {
                if (err.code == "ENOTFOUND") {
                    console.log(`Couldn't establish DB connection after ${counter}/${DatabaseManager.retryCount} retries.`)
                    return await new Promise((res, rej) => {
                        setTimeout(async () => {
                            res(await DatabaseManager.connect(counter + 1))
                        }, 2000)
                    })
                } else {
                    throw err;
                }
            }
            console.log(`Successfully established DB connection after ${counter} retries.`)
            DatabaseManager.isConnected = true;
            return pool;
        } else {
            console.error("Couldn't establish database connection after 5 retries.")
            process.exit(-1);
        }
    }


    static async openConnection():boolean {
        if (DatabaseManager.isConnected == false) {
            this.pool = await DatabaseManager.connect(0);
            return true;
        } else {
            return true;
        }
    }

    static connected():boolean {
        return DatabaseManager.isConnected;
    }

    static async resetDb() {
        if (DatabaseManager.connected()) {
            await new Pool().query("DELETE FROM users;");
            console.log("Reset database successfull.")
        }
    }
}

export default DatabaseManager;