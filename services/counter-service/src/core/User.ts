class User {
    public id: string;
    public clickCount: number;

    public constructor(id: string, clickCount: number) {
        this.id = id;
        this.clickCount = clickCount;
    }

    /**
     * Increments the current counter by 1 and returns the updated counter.
     */
    public incrementCount(): number {
        return this.clickCount++;
    }

}

export default User;