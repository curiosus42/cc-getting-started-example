import { gql } from 'apollo-server';

const typeDefs = gql`
    extend type User @key(fields: "id") {
        id: String! @external
        clickCount: Int
    }
`;

export default typeDefs;