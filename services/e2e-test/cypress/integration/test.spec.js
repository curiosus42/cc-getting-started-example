describe("Authentication", () => {
  it("can visit index page", () => {
    cy.visit("/");
  });

  it("can login using username and password", () => {
    cy.visit("/");
    cy.get('input[name="username"]').type("Benedikt Gack");
    cy.get('input[name="password"]').type("secret");
    cy.get('button[type="submit"]').click();
  });

});
