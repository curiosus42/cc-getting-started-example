<!-- PROJECT SHIELDS -->
<!--
*** Markdown Guidelines
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->

# How to - Uni-Project mit git

## Idee

Bei einem erfolgreichen Uni-Projekt (nicht nur) in der Software-Entwicklung ist die einfache Zusammenarbeit einer, wenn nicht sogar DER zentrale Punkt für den Erfolg. Was gibt es nervigeres als Merge-Konflikte oder versehentlich gepushte Änderungen, die das Programm zum Crashen bringen...

## Tools

- [**gitlab**](https://gitlab.mi.hdm-stuttgart.de/)
- [**git Bash**](https://gitforwindows.org/) oder [**Sourcetree**](https://www.sourcetreeapp.com/)

## Settings

Im Git-Repository gehen wir zunächst in die Projekteinstellungen und aktivieren **Merge Requests**, sowie **Pipelines**. Dadurch erscheint in der Seitenleiste ein neuer Eintrag **Merge-Requests** und die Möglichkeit einer CI/CD-Pipeline.

## Vorgehen für die erfolgreiche Zusammenarbeit

Um nun produktiv zusammenzuarbeiten, geht man folgenderweise vor:

1. Lokal den Master (oder Development-Branch) auschecken über `git checkout master`
2. Lokal einen neuen Branch erstellen und ihm einen aussagekräftigen Namen geben. Als gängige Praxis eignet es sich beispielsweise `<Kürzel>/<Ticket-Nummer>-<Feature>` als Name für den Branch. Der `/` bewirkt dabei eine Aufteilung in eine Ordnerstruktur nach Bearbeiter. Der passende Befehl für die git Console lautet `git checkout -b <feature_branch>`
3. Nun kann man mit Coden loslegen, Bugs fixen und neue Features entwickeln und lokal Änderungen machen, ohne sich Sorgen zu machen.
4. Nach getaner Arbeit eine sinnvolle commit-Message angeben und alle, bzw. nur die wirklich gewollten Änderungen committen. Der Befehl dazu lautet `git commit -a -m "Commit-Nachricht"`
5. Über den Befehl `git push origin <feature_branch>` fügt man seine Änderungen zum neuen Branch hinzu.
6. Ins Web-Interface von Git gehen und unter **Merge-Requests** einen neuen Merge-Request stellen. An diesen lassen sich neben einer anderen Commit-Message auch Screenshots und Dateien anhängen, um bspw. das neu entwickelte Feature zu verdeutlichen, denn Bilder sagen meistens mehr als 1000 Worte.
7. Die Kollegene können nun einfach im Web-Interface die Änderungen ansehen, Kommentare und Änderungswünsche hinterlassen und bei Zustimmung den Merge-Request über **genehmigen**.
8. Nachdem genügend Kollegen (1/2/...) den Merge-Request genehmigt haben, kann man ihn in dem Master (oder Develop-Branch) mergen.
9. Nach erfolgreichem Merge und Projekt-Build *sollte* man den Feature-Branch wieder löschen, um so sein Repository sauber zu halten.

Über die Software [**Sourcetree**](https://www.sourcetreeapp.com/) lassen sich die Schritte 1-5 simpel über eine GUI zusammenklicken, wenn man nicht so gerne über die Konsole arbeiten möchte.
__________________________________________________________________________

