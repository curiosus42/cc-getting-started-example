import Leaderboard from '../core/Leaderboard';
import User from '../core/User';
import { Pool } from 'pg';

import { UserNotFoundError } from '../core/Errors';

class LeaderboardMapper {
    private pool:Pool;

    constructor(pool:Pool) {
        this.pool = pool;
    }

    public async get(): Promise<Leaderboard> {
        const result = await this.pool.query("SELECT userId, clickCount FROM users ORDER BY clickCount DESC LIMIT 10");
        console.log("Leaderboard", result)
        return new Leaderboard(result.rows.map(usr => new User(usr.userid, usr.clickcount)));
    }

    public async getRank(userId: string): Promise<Number> {
        const result = await this.pool.query(
            "SELECT userId, clickCount, ROW_NUMBER () OVER (ORDER BY clickCount) as rank FROM users WHERE userId = $1",
            [userId]);
        if (result.rows[0]) {
            return result.rows[0].rank;
        } else {
            throw new UserNotFoundError(userId);
        }
    }
}

export default LeaderboardMapper;