import { ApolloServer } from 'apollo-server';
import { buildFederatedSchema } from '@apollo/federation';
import typeDefs from './graphql/typeDefs';
import resolvers from './graphql/resolvers';
import mongoose from 'mongoose';
import { IUser } from './models/User';

import insertScript from './insertScript';

function connect() {
    mongoose.connect('mongodb://' + process.env.DB_HOST + "/data", {useNewUrlParser: true});
}
connect();

const database = mongoose.connection;
var retryCount = 0;
database.on('error', (err) => {
    if (retryCount <= 5) {
        console.log(`Couldn't establish connection to User DB after ${retryCount}/5 retries.`);
        retryCount++;
        setTimeout(connect, 4000);
    } else {
        console.error("Couldn't establish connection to User DB after 5 retries. Exit")
        process.exit(-1);
    }
});
database.once('open', function() {
    console.log(`Successfully established connection to User DB after ${retryCount} retries.`);
    insertScript().then().catch(err => console.error(err));
});


console.log("Resolvers:", resolvers)

export interface Context {
    user: IUser;
}

const server = new ApolloServer({
    schema: buildFederatedSchema({ typeDefs, resolvers }),
    context: ({ req, res}) => {
        //Extracting the current user from the request header
        const user:string|undefined = req.get("user");
        let context:Context = {} as Context;
        if (user) {
            try {
                context.user = JSON.parse(user);
            } catch(err) {
                console.error("Invalid user context", err);
                return;
            }
            return context;
        } else {
            console.log("No user context provided.")
        }
    }
    });

    server.listen({port: process.env.PORT}).then(({ url }) => {
    console.log(`🚀 Server ready at ${url}`);
});