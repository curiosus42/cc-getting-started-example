import mongoose from 'mongoose';

import insertScript from '../src/insertScript';
import User from '../src/models/User';

class DatabaseManager {

    private static retryCount = 1;

    private static connect() {
        mongoose.connect('mongodb://' + process.env.DB_HOST + "/data", {useNewUrlParser: true});
        console.log("connect")
    }

    static openConnection():Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            if (DatabaseManager.connected()) {
                resolve(true);
            } else {
                const database = mongoose.connection;
                database.on('error', (err) => {
                    setTimeout(DatabaseManager.connect, 500);
                    console.error(`Couldn't connect to pinboard DB. Retry ${DatabaseManager.retryCount} / 5`);
                    if (DatabaseManager.retryCount <= 5) {
                        reject(err);
                    }
                });
                database.once('open', async function() {
                    console.log("CONNECTED to mongoDB.");
                    await DatabaseManager.resetDb();
                    resolve(true);
                });
                DatabaseManager.connect();
            }
        });
    }

    static connected():boolean {
        if (mongoose.connection.readyState == 1) {
            return true;
        } else {
            return false;
        }
    }

    static async resetDb() {
        if (DatabaseManager.connected()) {
            await User.deleteMany({});
            console.log("Reset database successfull.")
        }
    }
    static async loadInsertScript() {
        await insertScript();
        console.log("Inserted default data.")
    }
}

export default DatabaseManager;