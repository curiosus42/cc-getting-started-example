import mocha from 'mocha';
import {strict as assert} from 'assert';
import DatabaseManager from './DatabaseManager';

import User from '../src/core/User';
import UserMapper from '../src/data-mappers/UserMapper';

describe("Data mappers", () => {
    before("Create db conenction", async () => {
        await DatabaseManager.openConnection();
        return
    })
    describe("# User mapper", () => {
        it("get user", async () => {
            const userMapper = new UserMapper(DatabaseManager.pool);
            const usr = await userMapper.get("c7025528-f47d-43a5-bcfe-e972835fb417");
            assert.ok(usr.clickCount != 0);
        })
        it("change user", async () => {
            const userMapper = new UserMapper(DatabaseManager.pool);
            const usr = await userMapper.get("c7025528-f47d-43a5-bcfe-e972835fb417");
            usr.clickCount = 42;
            await userMapper.save(usr);
            
            const usr2 = await userMapper.get("c7025528-f47d-43a5-bcfe-e972835fb417");
            assert.strictEqual(usr2.clickCount, 42);
        })
    })
});