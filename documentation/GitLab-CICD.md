 <!-- PROJECT SHIELDS -->
<!--
*** Markdown Guidelines
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->

<!-- HEADER -->
<p align="center">
  <h1 align="center"> How to - Uni Project with CI/CD Pipeline </h1>
</p>


<!-- TABLE OF CONTENTS -->
## Table of Contents

- [Table of Contents](#table-of-contents)
- [The GitLab CI/CD Pipeline](#the-gitlab-cicd-pipeline)
    - [The three Continuous Methodologies](#the-three-continuous-methodologies)
    - [The CI Architecture](#the-ci-architecture)
    - [Connection of Continuous Integration, Continuous Development and Continuous Deployment](#connection-of-continuous-integration-continuous-development-and-continuous-deployment)
- [Types of Pipelines](#types-of-pipelines)
    - [The Pipeline Architecture](#the-pipeline-architecture)
- [Containerization of Pipeline Jobs](#containerization-of-pipeline-jobs)
- [Defining the Pipeline Jobs](#defining-the-pipeline-jobs)
    - [Structure of a Job](#structure-of-a-job)
- [Caching Dependencies](#caching-dependencies)
    - [Cache vs artifacts:](#cache-vs-artifacts)
    - [Job Artifacts](#job-artifacts)
- [GitLab Runner](#gitlab-runner)
- [Optimizing a Pipeline](#optimizing-a-pipeline)
- [Linting the Ci/CDPipeline](#linting-the-cicdpipeline)
- [Monitoring and Logging](#monitoring-and-logging)
- [Further Reading](#further-reading)

<!-- HEADER -->
<p align="center">
  <h1 align="center"> The GitLab CI/CD Pipeline </h1>
</p>

## The GitLab CI/CD Pipeline
A GitLab CI/CD Pipeline is configured through a YAML file which is named **.gitlab-ci.yml** and remains within a project’s root directory. The .gitlab-ci.yml file defines the structure and order of the pipeline jobs which are then executed sequentially or in parallel by a [GitLab Runner(https://docs.gitlab.com/runner/). For a quick try you can simply use the already available _Shared Runners_ which are hosted and provided by GitLab. The Runner tags are already provided in the <a href="https://gitlab.mi.hdm-stuttgart.de/schule-4.0/getting-started-example/.gitlab-ci.yml">CI/CD Pipeline Example</a>

#### The three Continuous Methodologies
Think about a pipeline as the top-level component of the three continuous methodologies. The three continuous methodology of _Continuous Integration (CI)_, _Continuous Delivery (CD)_ and _Continuous Deployment (CD)_ are based on the automation of execution steps through shell commands and scripts. The goal is to automate as much as possible so that errors or bugs can be avoided and teams can concentrate on the essential tasks. 

#### The CI Architecture
<p align="center"> 
  ![GitLab Linter image](./images/ci_architecture_overview.png "Linting00") 
</p>

#### Connection of Continuous Integration, Continuous Development and Continuous Deployment
An outflow of the pipeline is structured in the following GitLab graphic.
<p align="center"> 
  ![GitLab Pipeline image](./images/gitlab_workflow_example_11_9.png "Pipeline00") 
</p>

## Types of Pipelines
Even that it is more common to have only one ```.gitlab-ci.yml``` file for many projects, a CI/CD Pipeline in the project's root can also orchestrate multiple other pipeline files in other directions. For example throught the parent-child pipeline architecture. This architecture might make sense if a mono repository contains multiple applications that have their own pipelines. Which pipeline architecture makes the most sense depends on the type of your project.

- **Basic pipelines** run everything in each stage concurrently, followed by the next stage. It is not very efficient, but the simplest version.
- **Directed Acyclic Graph Pipeline (DAG)** pipelines are based on relationships between jobs and can run more quickly than basic pipelines.
- **Multi-project pipelines** combine pipelines for different projects together.
- **Parent-Child pipelines** break down complex pipelines into one parent pipeline that can trigger multiple child sub-pipelines, which all run in the same project and with the same SHA.
- **Pipelines for Merge Requests** run for merge requests only (rather than for every commit).

#### The Pipeline Architecture

The **basic pipeline** architecture shown in the following is not very efficient, but easiest to maintain. As such it is shown, in combination with the following ```.gitlab-ci.yml``` file example, as a basic example, to understand the construction of a pipeline's architecture. 
<p align="center"> 
  ![GitLab Linter image](./images/pipeline_architecture_basic.png "Linting00") 
</p>

```
# default image
image: node:alpine

stages: 
  - build 
 - test 
  - deploy  

# -----  CI ------ #
build_a: 
       stage: build 
       script: - echo "This job builds something." 
       ....

build_b: 
     stage: build 
     script: - echo "This job builds something else." 
     ....

test_a: 
    stage: test 
    script: - echo "This job tests something."
    ....

test_b: 
     stage: test 
     script: - echo "This job tests something else."
     ....

# ----- CD ------ #
deploy_a: 
     stage: deploy 
     script: - echo "This job deploys something." 
     ....

deploy_b: 
     stage: deploy 
     script: - echo "This job deploys something else."
     ....
```
## Containerization of Pipeline Jobs
It is useful to use Docker to run scripts and deploy the project. Automating containerized jobs standardizes the execution and ensures that no errors slip through due to different environments in which build, and tests are executed. Docker images can either be found on DockerHub or if maintaining own images pulled from a container registry. 

The docker default image belongs into the first line of the ```.gitlab-ci.yml```. It makes sense to use a possibly slim image, for example Alpine Linux.

```
# default image
image: node:alpine

```

## Defining the Pipeline Jobs
Pipelines contain _jobs_ that determine what is needed to do and _stages_ which define when the jobs should be executed. In the pipeline's first lines the stages for jobs are defined via the ```stages``` tag. Jobs with the same stage-tag are executed in parallel. A typical pipeline might consist of four stages, executed in the following order:

- **build** - code is executed and built.
- **test** - code tests, as well as quality checks by means of linting etc.
- **staging** - deploy the code to staging.
- **deploy** - deploy the code to production.

```
stages:
  // Continuous Integration - CI Blocks
  - install
  - build
  - quality
  - test
  // Continuous Development + Deployment - CD Blocks
  - staging
  - production
```

All jobs can be reduced or further extended, as required for the project. In the above example the standard jobs were extended through an install, quality and job. 
More about the tags can be found under the official [**GitLab Documentation - CI Pipelines**](https://docs.gitlab.com/ee/ci/pipelines/).

#### Structure of a Job

The basic structure of the individual jobs within a pipeline includes the block name, and subordinate stage with stage tag, script with the executable scripts/commands. Each job must also contain a tag for the selected runner (here: ``gitlab-runner-shell``). Each job can hold a tag for a different runner, which is assigned to a GitLab project. In some cases it might make sense to split jobs between a runner that only executes CI and another runner that only executes CD jobs. 
```
job_name:
  stage: 
    - build
  script:
    - echo "Doing something important."
    - //
  tags:
    - gitlab-runner shell
```
Under a scripts-tag you can add all commands that you could also execute in a shell-console. The order of the elements within a block is not predefined. It is only required to keep the structure of _key - value_ pairs, for example ```stage: job-stage-tag```

Jobs can be started in different ways. This depends on the tags you assign to the individual blocks. For example, it can make sense that a deploy job receives a tag for a manual job and thus forces the job to wait for a manual release by a developer. This can be done using the when and only tags, as in the following example:

```
 when: manual
    only:
      - staging
      - merge_requests
```
A manual release takes place in GitLab by clicking the Run button. In addition, certain restrictions can be set, such as the ``- merge_requests'' which specifies that this job can only be executed when a merge request is made to the Master Branch.


## Caching Dependencies
When caching is enabled, it is used to cache shared dependencies between pipelines and jobs at the project level.

```
cache:
  key: ${CI_COMMIT_REF_SLUG}
  path: 
    - node_modules 
``` 

#### Cache vs artifacts:
- **cache:** For storing project dependencies
- **artifacts:** Use for stage results that will be passed between stages.

#### Job Artifacts
For team collaboration, additional artifacts can be useful, which can be downloaded after each successfully completed pipeline block. This enables designers to work better with developers, because executable builds (e.g. installable APK files for Android applications), screenshots or code coverage reports (html, xml, pdf etc.) can be downloaded directly without the need of the actual source code.

This can also be done in one block. A path under which elements should be stored and their expiration time can be specified additionally. More information can be found at [**GitLab Documentation Job Artefacts**](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html).

Example:
```
artifacts:
      paths:
         - build
      expire_in: 30 mins
```

## GitLab Runner
The jobs are executed using a runner. More information on installing and using runners can be found at [**Gitlab Documentation - Runner**](https://docs.gitlab.com/runner/).
You can also get an overview of the runners currently available for your project by going to **Project > Settings > CI/CD > Expand Runner** tab in GitLab. A runner gets tags that can be specified within a block in the CI/CD pipeline. 

Example: 
```
tags:
    - gitlab-runner shell
```

## Optimizing a Pipeline

<!-- wp:paragraph -->
<p>Efficiency and speed are very important when running the jobs through a CI/CD Pipeline. Therefore it is important to think not only about the architecture but also about the following concepts.</p>
<!-- /wp:paragraph -->

<!-- wp:list -->
<ul><li><strong>Host an own GitLab Runner</strong>: Often the bottleneck is not necessarily the hardware, but the network. Whilst GitLab's Shared Runners are quick to use the network of a private cloud server is faster.</li><li><strong>Pre-install dependencies</strong>: Downloading all needed dependencies for each CI job is tedious and takes a lot of time. It makes sense to pre-install all dependencies on an own docker image and push it to a <a href="https://docs.gitlab.com/ee/user/packages/container_registry/">container registry</a> to fetch it from there. Another possibility is to cache dependencies locally.</li><li><strong>Use slim docker images:</strong> Rather use a tiny Linux distribution for images to execute a CI job than a fully blown up one, which you might not even need. We, therefore, used an <a href="https://alpinelinux.org/">Alpine Linux</a> distribution.  &nbsp;</li><li><strong>Cache dynamic dependencies:</strong> If dependencies need to be dynamically installed during a job and thus can't be pre-installed in an own docker image. GitLab’s cache possibility allows to<a href="https://docs.gitlab.com/ee/ci/caching/"> cache dependencies</a> between job runs.</li><li><strong>Only run a job if relevant changes happened: </strong>This is very useful for a  microservice architecture. For example, if the front-end changed, the build and test jobs for all the others don't need to run as well. This can be achieved by using the <a href="https://docs.gitlab.com/ee/ci/yaml/"><code>onl</code>y keyword</a> in a job.</li></ul>
<!-- /wp:list -->


## Linting the Ci/CDPipeline

To reduce time-consuming errors in the script, GitLab also allows for linting, as well as an interactive Web IDE for early exploration.
[**GitLab Documentation - Code Quality**](https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html)

Using GitLab Linters, the script can be checked for syntactical errors and corrected before execution.
<p align="center"> 
  ![GitLab Linter image](./images/ci_lint.png "Linting00") 
</p>


## Monitoring and Logging
Once the pipeline is running, it can be viewed in GitLab under **CI/CD > Pipelines**. If the execution of a job fails, it is marked as failed. There are mainly the states successfull, paused and failed. If a job fails, all subsequent jobs will not be executed and the pipeline will stop. In the details view, you can download not only log files but also previously defined artifacts (quality reports, screenshots, build files) returned by the GitLab Runner.

View jobs through **Pipeline > Jobs**
![GitLab Pipeline Status image](./images/pipelines_status.png "Pipeline02")

If you navigate to the individual jobs, you get a detailed view of each executed job through log files, which are returned by the Runner. This is important to diagnose why a job failed or acted differently than you expected. These can look like this: 
![GitLab Pipeline Log image](./images/build_log.png "Log File")


----------------------------------------------------------------------------

## Further Reading
<!-- wp:heading {"level":3} -->
<h3><strong>CI/CD Pipeline</strong></h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p><strong>Continuous Integration:</strong><br>Definition of Continuous Integration - Martin Fowler<br><a href="https://martinfowler.com/articles/continuousIntegration.html/">https://martinfowler.com/articles/continuousIntegration.html/</a><br><br><strong>Continuous Delivery:</strong><br>Continuous Delivery - Martin Fowler<br><a href="https://www.martinfowler.com/bliki/ContinuousDelivery.html">https://www.martinfowler.com/bliki/ContinuousDelivery.html</a><br>Architecture - Jez Humble <a href="https://continuousdelivery.com/implementing/architecture/">https://continuousdelivery.com/implementing/architecture/</a><br>Principles - Jez Humble <br>https://continuousdelivery.com/principles/</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Continuous Deployment:</strong><br>Continuous Deployment Blog - Timothy Fitz <a href="http://timothyfitz.com/2009/02/08/continuous-deployment/">http://timothyfitz.com/2009/02/08/continuous-deployment/</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>GitLab CI/CD:</strong><br>Rollbacks - GitLab Docs <a href="https://docs.gitlab.com/ee/ci/environments/#retrying-and-rolling-back">https://docs.gitlab.com/ee/ci/environments/#retrying-and-rolling-back</a><br>Incremental Rollouts - GitLab Docs <a href="https://docs.gitlab.com/ee/ci/environments/incremental_rollouts.html">https://docs.gitlab.com/ee/ci/environments/incremental_rollouts.html</a><br>CI Runner - GitLab Docs <br><a href="https://docs.gitlab.com/ee/ci/runners/README.html">https://docs.gitlab.com/ee/ci/runners/README.html</a><br>Pipeline Linting - GitLab Docs<br><a href="https://docs.gitlab.com/ee/ci/lint.html">https://docs.gitlab.com/ee/ci/lint.html</a><br>CI/CD Job Logging - GitLab Docs<br><a href="https://docs.gitlab.com/ee/administration/job_logs.html">https://docs.gitlab.com/ee/administration/job_logs.html</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Containerization and virtualization:</strong><br>Docker: <a href="https://www.docker.com/">https://www.docker.com/</a><br>Docker integration - GitLab Docs<br><a href="https://docs.gitlab.com/ee/ci/docker/README.html">https://docs.gitlab.com/ee/ci/docker/README.html</a><br>Vagrant virtualization - Vagrant<br><a href="https://www.vagrantup.com/">https://www.vagrantup.com/</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Runner installation:</strong><br>AWS EC2 Instance <br><a href="https://aws.amazon.com/de/getting-started/">https://aws.amazon.com/de/getting-started/</a><br>Runner installation on RaspberryPi - Sebastian Martens<br><a href="https://blog.sebastian-martens.de/technology/install-gitlab-runner-on-raspberry-pi/">https://blog.sebastian-martens.de/technology/install-gitlab-runner-on-raspberry-pi/</a><br>GitLab Runner <br><a href="https://docs.gitlab.com/runner/">https://docs.gitlab.com/runner/</a></p>
<!-- /wp:paragraph -->