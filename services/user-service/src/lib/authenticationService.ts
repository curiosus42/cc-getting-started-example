import axios from 'axios';

const authenticationServiceApi = process.env.AUTHENTICATION_API;

if (!authenticationServiceApi) throw new Error("AUTHENTICATION_API environment variable is missing.")

/**
 * Represents the internal service API of the authentication-service
 */
class AuthenticationService {

    private isOnline = false;

    public constructor() {
        this.checkOnline();
    }

    /**
     * Updates the user in the authentication-service with the new username.
     * @param {string} userId 
     * @param {string} name 
     * @return {boolean} whether the user was updated successfully or not.
     */
    public async updateUsername(userId:string, name:string) {
        if (await this.checkOnline()) {
            const res = await axios.get(authenticationServiceApi + "/user/" + userId, {
                params: {
                    name: name
                },
                timeout: 500 //wait at maximum 500ms for a response
            });

            return res.status == 200;
        } else {
            return false;
        }
    }

    private async checkOnline() {
        if (this.isOnline) {
            return true;
        } else {
            try {
                const res = await axios.get(authenticationServiceApi + "/check");
                if (res.status == 200) {
                    this.isOnline = true;
                    return true;
                } else {
                    return false;
                }
            } catch (err) {
                return false;
            }
        }
    }

}

export default AuthenticationService;