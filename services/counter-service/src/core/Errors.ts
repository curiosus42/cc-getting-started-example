export class UserNotFoundError extends Error {
    constructor(userId:string) {
        super(`The requested user ${userId} doesn't exist.`)
        this.name = "UserNotFoundError";
    }
}