import { Context } from '../../../../start-service';
import User from '../../../../core/User';
import UserMapper from '../../../../data-mappers/UserMapper';

interface Parent {
    id:string
}

const clickCountResolver = async (parent:Parent, args:void, context:Context, info:any): Promise<Number | null> => {
    let usrMapper = new UserMapper(context.pool);
    let user = await usrMapper.get(parent.id);

    return user.clickCount;
}

export default clickCountResolver;