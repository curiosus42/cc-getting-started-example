import user from './User';

const mutations = {
    ...user,
}

export default mutations;