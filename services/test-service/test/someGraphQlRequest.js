import pkg from 'graphql-request';
import http from 'http';
import assertion from 'assert';
const assert = assertion.strict;
const { GraphQLClient, gql } = pkg;

function checkAvail(count) {
    return new Promise((resolve, reject) => {
        if (count < 5) {
            http.get(process.env.GATEWAY + "/.well-known/apollo/server-health", (resp) => {
                let data = '';
                
                resp.on('data', (chunk) => data += chunk);
                
                resp.on('end', () => {
                    const json = JSON.parse(data);
                    assert.ok(json.status == "pass");
                    console.log("API Gateway available.")
                    resolve(true)
                });
                
            }).on("error", (err) => {
                setTimeout(checkAvail.bind(null, count + 1), 1000);
                console.log("Couldn't connect to gateway.", count)
            });
        } else {
            resolve(false)
        }
    })
}


describe("Request the click count from the user 'Benedikt Gack'", () => {

    before("api gateway available", async () => {
        const avail = await checkAvail(0);
        console.log("Available?", avail);
        assert.strictEqual(avail, true);
    })

    it("api-gateway is available", (done) => {
        
        http.get(process.env.GATEWAY + "/.well-known/apollo/server-health", (resp) => {
            let data = '';
            
            resp.on('data', (chunk) => data += chunk);
            
            resp.on('end', () => {
                const json = JSON.parse(data);
                assert.ok(json.status == "pass");
                done()
            });
            
        }).on("error", (err) => done(err));
    })

    var token;

    describe("#login", () => {
        it("Should return a valid JWT", async () => {
            const graphQLClient = new GraphQLClient(process.env.GATEWAY + "/graph");
            const loginMutation = gql`
                mutation {
                    login(username: "Benedikt Gack", password: "secret")
                }
            `;
            const response = await graphQLClient.request(loginMutation);
            assert.ok(response.login);
            console.log(response.login);
            token = response.login;
        })
    })

    describe("#Request click count", () => {
        it("Returned click-count for Benedikt should be <11 by default", async () => {
            const graphQLClient = new GraphQLClient(process.env.GATEWAY + "/graph", {
                headers: {
                    authorization: `Bearer ${token}`,
                }
            });
            const countQuery = gql`{
                me {
                    username
                    clickCount
                }
            }`;

            const response = await graphQLClient.request(countQuery);
            assert.ok(response.me);
            const me = response.me;
            assert.strictEqual(me.username, "Benedikt Gack")
            assert.ok(me.clickCount >= 12)
        })
    })
})