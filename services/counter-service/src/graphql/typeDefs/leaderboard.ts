import { gql } from 'apollo-server';

const typeDefs = gql`
    type Leaderboard {
        """
        Returns the top ten users by click count.
        """
        topTen: [ User ]!
    }

    extend type Query {
        leaderboard: Leaderboard
    }
`;

export default typeDefs;