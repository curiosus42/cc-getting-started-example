# authentication-service- serviceApi

## Abstract

Internal microservice API for updating the login name in the authentication service.

## GET /check
Returns 200 if the service API is online.

## GET /updateUsername/[userId]

**Request Parameters:**
* userId: the id of the user to update

**Request Query:**
* name: String (the updated username)

**Returns:**
Status code 200 if success, !=200 otherwise.