import { gql } from "apollo-server";

const mTypeDefs = gql`

  type Mutation {
    """
    Creates a new user and returns a JWT Token.
    """
    signup(username: String!, password: String!): String
  }
`;

export default mTypeDefs;