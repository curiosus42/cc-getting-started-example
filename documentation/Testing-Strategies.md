# How to test a microservice architecture?
Throughout this example project you will find various tests. To get started, the following pyramid shows the different levels of testing and how we adressed them:

![Scott Logic testing strategies](./images/testing-strategies.jpg)

## E2E
The simplest approach for testing the whole plattform is to mimic a user. Therefore we used the front-end testing framework cypress, which remote controls a web browser and executes pre defined tasks.

Try it on your own by running `docker-compose -f docker-compose.e2e.yml up` from the root folder.

## Integration
The goal is to test the ensemble playing of mutliple parts in your architecture. In this repository we demonstrated how to test that all services are working togetehr coorectly by making test GraphQL requests to the graphql-gateway.

Try it on your own by running `docker-compose -f docker-compose.dev.yml up` from the root folder. 

## Service
A Service in our example consists of a service app and a database. The hardest task is to make sure, that the service is up and healthy before running the tests. Therefore we made sure, that each service is connected to the database before running the tests.

Because we only want to test a single service we have to mock additional dependencies to other services. To demonstrate that we added a `rename` mutation to the user-service, which will only update the display name. In order for a user to login with his new name, the authentication-service needs to be notified about the event. Therefore we added a service api to those services which is only available internally and exposed by port.

In order to mock the internal service api we used `axios` as a request library and the `axios-mock-adapter` for mocking outgoing requests by responding with predefined data.

Try it on your own by running `docker-compose -f docker-compose.dev.yml up` in the services folders.

## Unit
These tests aim to test functionality of a service, which does not depend on external dependencies. We used `mocha` as a testing framework.

Try it on your own by running `docker-compose -f docker-compose.dev.yml up` in the services folders.

## Further reading:

Testing Strategies in a Microservice Architecture - Toby Clemson\
https://martinfowler.com/articles/microservice-testing/

Continuous Testing – Creating a testable CI/CD pipeline. - Scott Logic\
https://blog.scottlogic.com/2020/02/10/continuous-testing.html