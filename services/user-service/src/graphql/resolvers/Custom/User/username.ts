import { Context } from '../../../../start-service';
import User from '../../../../models/User';

interface Parent {
    id:string
}

const usernameResolver = async (parent:Parent, args:void, context:Context, info:any): Promise<Number | null> => {
    const user = await User.findOne({id: parent.id}).exec();
    return user.username;
}

export default usernameResolver;