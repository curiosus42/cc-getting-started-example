import User from "#root/models/User";

/**
 * Updates the login name for a given user.
 * @param {Express Router} router 
 */
function updateUsername(router) {
    router.get('/user/:id', async (req, res) => {
        if (!req.params.id || !req.query.name) {
            return res.status(400).send("Invalid request.");
        }
        const result = await User.updateOne({id: req.params.id}, {username: req.query.name});
        if (result.n == 1) {
            console.log("Successfully updated login username.", result);
            return res.status(200).send();
        } else {
            return res.status(404).send("Requested user not found.");
        }
    })
}

export default updateUsername;