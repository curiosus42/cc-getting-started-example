import express from 'express';
import setupRoutes from './routes';

function startServiceApi() {
    const app = express();

    //add api routes to express app (.body)
    setupRoutes(app);

    app.listen(process.env.API_PORT, () => {
        console.log(`⚡️[server]: API Server is running at http://localhost:${process.env.API_PORT}`);
    });
}

export default startServiceApi;