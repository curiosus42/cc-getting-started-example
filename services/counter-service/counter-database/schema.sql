/*
    CREATE TABLE users
*/

DROP TABLE IF EXISTS users;

CREATE TABLE users (
    userId      varchar(40) PRIMARY KEY,
    clickCount  integer DEFAULT 0
);


/*
    INSERT TESTDATA INTO users
*/

INSERT INTO users (userId, clickCount) VALUES
    ('c7025528-f47d-43a5-bcfe-e972835fb417', 12)
    ,('540c2778-60c7-4911-9238-1460710018a0', 7)
    ,('640c2778-60c7-4911-9238-3333330018a0', 22);