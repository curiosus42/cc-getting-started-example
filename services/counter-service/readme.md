# counter-service
This service represents the core business logic of our example application. It associates a user with his click count and calculates the Leaderboard.

## Building
Note that this service is written in typescript and needs to be compiled (transcompiled) and bundled in order to be deployed to a server. The reason we did that is to demonstrate how to deal with applications, which can not be executed right away. For development we're using `babel-node` which is configured to transpile the code on the fly.

For building the appliaction run `npm run build`.

## Project structure

### \src\core
This folder contains the core objects of the service, which are a *User* and the *Leaderboard* class. These classes contain our business logic which is basically nothin more then incrementing a counter by one.

### \src\data-mappers
This folder contains the logic which creates and stores our core objects. Therefore it directly communicates with the *postgres* database.

### \counter-database
This folder contains the Dockerfile for creating our custom database *postgres* image, which is pre-configured with our SQL schema and some default data.

### \src\graphql
Implementations and definitions for the graph API.

## Key features
* Manage user click counts
* Calculate the Leaderboard

## Dependencies
* Postgres database