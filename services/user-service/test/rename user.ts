import {strict as assert} from 'assert'
import mongoose from 'mongoose';

import renameMutation from '../src/graphql/resolvers/Mutations/User/rename';
import User from '../src/models/User';

import MockAdapter from 'axios-mock-adapter';
import axios from 'axios';

import DatabaseManager from './DatabaseManager';

const mock = new MockAdapter(axios);


describe("Check rename resolver", () => {
    before("Create db conenction", async () => {
        DatabaseManager.connected() ? true : await DatabaseManager.openConnection();
        await DatabaseManager.resetDb();
        await DatabaseManager.loadInsertScript();
        return
    })

    //mock the requests to the autghentication service
    mock.onGet("http://authentication-service:4002/check").reply(200, true);
    mock.onGet(/http:\/\/authentication-service:4002\/user\/.*/gm).reply(200, true);
    it("User renamed successfully", async () => {
        const userContext = {
            user: {
                id: "c7025528-f47d-43a5-bcfe-e972835fb417",
                username: "Benedikt Gack"
            }
        }
        const result = await renameMutation(null, {username: "Hans"}, userContext);
        assert.strictEqual(result, true);

        //check if the name was updated in the database
        const usr = await User.findOne({id:"c7025528-f47d-43a5-bcfe-e972835fb417"});
        assert.strictEqual(usr.username, "Hans")
    })
})