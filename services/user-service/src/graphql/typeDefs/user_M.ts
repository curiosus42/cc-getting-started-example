import { gql } from 'apollo-server';

const typeDefs = gql`
    type Mutation {
        """
        Renames the current user. Also changes the login name.
        """
        rename(username:String):Boolean
    }
`;

export default typeDefs;