Dieser Service sendet einfache grahql queries an das gateway.

Das Testframework mocha wird verwendet.

Query Beispiel mit fetch:

    fetch('/graphql', {
    method: 'POST',
    headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
    },
    body: JSON.stringify({query: "{ hello }"})
    })
    .then(r => r.json())
    .then(data => console.log('data returned:', data));