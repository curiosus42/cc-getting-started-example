import React, {Component} from 'react';
import './layout.css';
import gql from "graphql-tag";
import GRAPHQL from "../lib/graphql"
import {ApolloProvider} from "@apollo/client";

class Login extends Component {

    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            error: '',
            success: '',
        };

        this.handlePassChange = this.handlePassChange.bind(this);
        this.handleUserChange = this.handleUserChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.dismissError = this.dismissError.bind(this);
    }

    login = () => {
        console.log("login");
        GRAPHQL
            .mutate({
                mutation: gql`
            mutation{
                login(
                    username: "${this.state.username}", 
                    password: "${this.state.password}"
                )
            }`
            })
            .then(result => {
                this.setState({success: 'Login successful'});
                //console.log(result.data);
                window.sessionStorage.setItem("token", result.data.login);
            })
            .catch(e => {
                //console.log(e);
                this.setState({success: ''})
                this.setState({error: 'login invalid'})
            });
    }


    dismissError = () => {
        this.setState({error: ''});
    }

    handleSubmit = (evt) => {
        evt.preventDefault();

        if (!this.state.username) {
            return this.setState({error: 'Username is required'});
        }

        if (!this.state.password) {
            return this.setState({error: 'Password is required'});
        }


        return this.setState({error: ''});
    }

    handleUserChange = (evt) => {
        this.setState({
            username: evt.target.value,
        });
    };

    handlePassChange = (evt) => {
        this.setState({
            password: evt.target.value,
        });
    }

    render() {

        return (
            <ApolloProvider client={GRAPHQL}>
                <div className="Login">
                    <form onSubmit={this.handleSubmit}>
                        {
                            this.state.error &&
                            <h3 onClick={this.dismissError}>
                                <button onClick={this.dismissError}>✖</button>
                                {this.state.error}
                            </h3>
                        }
                        {
                            this.state.success &&
                            <h3>
                                {this.state.success}
                            </h3>
                        }
                        <label>Username:
                            <input type="text" name="username" value={this.state.username}
                                   onChange={this.handleUserChange}/>
                        </label>
                        <label>Password:
                            <input type="password" name="password" value={this.state.password}
                                   onChange={this.handlePassChange}/>
                        </label>
                        <input type="submit" value="Login" onClick={this.login}/>
                    </form>
                </div>
            </ApolloProvider>
        );
    }
}

export default Login;