import { gql } from 'apollo-server';

import user from './ext_user';
import userM from './ext_userM';
import leaderboard from './leaderboard';

const typeDefs:any = gql`
    ${user}
    ${userM}
    ${leaderboard}
`

export default typeDefs;