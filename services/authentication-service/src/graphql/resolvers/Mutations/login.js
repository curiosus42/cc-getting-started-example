import User from "#root/models/User";
var jwt = require("jsonwebtoken");

const JWT_SECRET = process.env.JWT_SECRET;

class UserNotFound extends Error {
  constructor(username) {
    super(`User with name "${username}" does not exist.`);
  }
}
class InvalidPassword extends Error {
  constructor(username) {
    super(`Invalid password for user "${username}"`);
  }
}

const loginResolver = {
  async login(_, { username, password }, { res }) {

    let user = await User.findOne({ username });
    if (!user) {
      throw new UserNotFound(username);
    }

    return new Promise((resolve, reject) => {
      user.comparePassword(password, (err, isMatch) => {
        if (err) throw err;
        console.log(password, isMatch);
        if (isMatch) {
          const token = jwt.sign(
            { username, id: user.id },
            JWT_SECRET,
            {
              algorithm: "HS256",
              expiresIn: "1d",
            }
          );
          resolve(token);
        } else {
          reject(new InvalidPassword(username));
        }
      });
    })
  }

};

export default loginResolver;