function check(router) {
    router.get('/check', (req, res) => {
        res.status(200).send("exercise-service internal API online.")
    })
}

export default check;