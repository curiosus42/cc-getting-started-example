import { Context } from '../../../../start-service';
import User from '../../../../core/User';
import UserMapper from '../../../../data-mappers/UserMapper';

const clickOnceResolver = async (parent:void, args:void, context:Context, info:any):Promise<Number> => {
    console.log("CLICK ONCE")
    let usrMapper = new UserMapper(context.pool);
    let user = await usrMapper.get(context.user.id);

    let newCount = user.incrementCount();
    await usrMapper.save(user);
    
    return newCount;
};

export default clickOnceResolver;