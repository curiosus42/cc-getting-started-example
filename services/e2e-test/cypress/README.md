# End to End Testing

With these Cypress tests we want to test the Application End to End.

By simulating the user inputs in the frontend and a connected backend all application parts can be teste together.

To run the tests install cypress and select `client-app` as the root folder for the tests.
Because our application runs completley in docker and we did not want to include cypress in our dependencies please install cypress locally.

Please make sure that all services are running with docker on port `80`.
