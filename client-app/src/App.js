import React, { useState } from 'react';

import LOGIN from "./components/login"
import { ApolloProvider } from "@apollo/client";
import gql from "graphql-tag";
import GRAPHQL from "./lib/graphql"

function getLeaderboard(setLeaderboard) {
    GRAPHQL
        .query({
            context: {
                headers: {
                    origin: '*',
                    authorization: `Bearer ` + window.sessionStorage.getItem("token") || null,
                }
            },
            query: gql`
          {
              leaderboard {
                  topTen {
                      username
                      clickCount
                  }
              }
          }`
        })
        .then(result => {
            console.log(result);
            setLeaderboard(result.data.leaderboard.topTen);
        }).catch(e => console.log(e));
}

function incCounter() {
    GRAPHQL
        .mutate({
            context: {
                headers: {
                    origin: '*',
                    authorization: `Bearer ` + window.sessionStorage.getItem("token") || null,
                }
            },
            mutation: gql`
              mutation {
                  clickOnce
              }
          `
        })
        .then(result => {
            document.getElementById("counter").innerText = result.data.clickOnce;
            console.log(result)
        }).catch(e => console.log(e));
}

function App() {
    const [leaderboard, setLeaderboard] = useState([]);

    return (
        <ApolloProvider client={GRAPHQL}>
            <h1>Hi people</h1>
            <p>Welcome to our test-page.</p>

            <LOGIN />
            <button onClick={() => getLeaderboard(setLeaderboard)}>
                Leaderboard anzeigen
            </button>
            <br />
            <button onClick={incCounter}>
                +1
        </button>
        Zähler: <span id="counter"></span>
            <table id="output">
                <tr>
                    <th>Username</th>
                    <th>clickCount</th>
                </tr>
                {leaderboard.map(entry => {
                    return (
                        <tr key={entry.username}>
                            <td>{entry.username}</td>
                            <td>{entry.clickCount}</td>
                        </tr>
                    )
                })}
            </table>
        </ApolloProvider>
    );
}

export default App;
