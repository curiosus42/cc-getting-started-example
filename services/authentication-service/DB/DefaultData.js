db.users.insertMany([
    {
        id: "c7025528-f47d-43a5-bcfe-e972835fb417",
        username: "Benedikt Gack",
    },
    {
        id: "540c2778-60c7-4911-9238-1460710018a0",
        username: "Yannick Möller",
    },
    {
        id: "540c2778-60c7-4911-9238-1460710018a0",
        username: "Janina Mattes",
    }
]);

db.users.createIndex({
    keys: {
        id: 1
    },
    options: {
        unique: true,
        name: "UserId index"
    },
})

print("INSERTED DATA!");