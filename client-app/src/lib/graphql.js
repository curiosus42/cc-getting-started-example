import {ApolloClient, InMemoryCache} from "@apollo/client";

let client = new ApolloClient({
    uri: 'http://localhost:4001/graph',
    cache: new InMemoryCache(),
});

export default client;