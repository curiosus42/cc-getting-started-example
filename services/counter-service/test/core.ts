import mocha from 'mocha';
import {strict as assert} from 'assert';
import DatabaseManager from './DatabaseManager';

import User from '../src/core/User';

describe("Core business logic", () => {
    describe("# User", () => {
        const usr = new User("c7025528-f47d-43a5-bcfe-e972835fb417", 12);
        it("counter increment", () => {
            assert.strictEqual(usr.clickCount, 12)
            usr.incrementCount();
            assert.strictEqual(usr.clickCount, 13)
        })
    })
});