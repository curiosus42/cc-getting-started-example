import User from './User';

class Leaderboard {
    public topTen: User[];
    public constructor(topTenUsers: User[]) {
        this.topTen = topTenUsers;
    }
}

export default Leaderboard;