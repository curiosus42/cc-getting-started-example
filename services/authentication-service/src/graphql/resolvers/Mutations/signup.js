import User from "#root/models/User";
var jwt = require("jsonwebtoken");

const JWT_SECRET = process.env.JWT_SECRET;

const signupResolver = {

  async signup(_, { username, password, role }, { res }) {
    try {
      let newUser = {
        username,
        password,
      };

      let user = new User(newUser);
      user = await user.save();

      const token = jwt.sign(
        { username: user.username, id: user.id },
        JWT_SECRET,
        {
          algorithm: "HS256",
          expiresIn: "1d",
        }
      );

      return token;
    } catch (err) {
      console.error(err);
      throw err;
    }
  },
};

export default signupResolver;