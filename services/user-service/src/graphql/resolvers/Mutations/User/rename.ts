import { Context } from '../../../../start-service';
import { IUser } from '../../../../models/User';
import User from '../../../../models/User';
import Mongoose from 'mongoose';

import AuthenticationService from '../../../../lib/authenticationService';

const authService = new AuthenticationService();

interface RenameInput {
    username:string
}

const renameResolver = async (parent: void, args:RenameInput, context:Context) => {
    const updateSuccessfull = await authService.updateUsername(context.user.id, args.username);
    console.log("updateSuccessfull", updateSuccessfull)
    if (updateSuccessfull) {
        const result = await User.updateOne({id: context.user.id}, {username: args.username});
        if (result.n == 1) {
            return true
        } else {
            return false
        }
    } else {
        return false;
    }
}

export default renameResolver;