import { gql } from 'apollo-server';

const typeDefs = gql`
    type User @key(fields: "id") {
        id: String!
        username: String!
    }

    type Query {
        """
        Represents the current user.
        Use this endpoint to query content related to the current user.
        """
        me: User
    }
`;

export default typeDefs;