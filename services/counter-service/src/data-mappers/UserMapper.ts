import User from '../core/User';
import { Pool } from 'pg';

import { UserNotFoundError } from '../core/Errors';


class UserMapper {
    private pool:Pool;

    constructor(pool:Pool) {
        this.pool = pool;
    }
    
    /**
     * 
     * @param userId 
     */
    public async get(userId: string): Promise<User> {
        const result = await this.pool.query('SELECT userId, clickCount FROM users WHERE userId = $1', [userId]);
        if (result.rows[0]) {
            return new User(result.rows[0].userid, result.rows[0].clickcount);
        } else {
            throw new UserNotFoundError(userId);
        }
    }

    /**
     * Updates an existing user or creates a new one.
     * @param user 
     */
    public async save(user: User): Promise<void> {
        const result = await this.pool.query(
            "INSERT INTO users (userId, clickCount) VALUES ($1, $2) ON CONFLICT (userId) DO UPDATE SET clickCount = EXCLUDED.clickCount", 
            [user.id, user.clickCount]);
    }
}

export default UserMapper;