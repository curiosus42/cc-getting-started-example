# user-service
This service represents the User in the data graph. Therefore it implements the type *User* which can be extended by other services with context specific data. It is also responsible for the */me* query.

## issue
We implemented a mutation called `rename` in order to demonstrate a technical limitation, which is explained in detail in our [blog post](https://blog.mi.hdm-stuttgart.de/index.php/2020/08/21/getting-started-with-devops/). This mutation changes the name of the user in this service. Because the authentication-service also stores the username for the login mechanism, the username needs to be synched across these two services with an internal service API, which is not available for the outer world.

## Service testing
Testing this service is not so easy, because it sends out http requests to the authentication-service for keeping the username in sync. For testing, these requests are catched and responded with predefined valid fake data. With this technique called *mocking*, we can test this service without running the authentication service.

## Key features
* Implements the type User, which can be extended by other services
* Communicates with the



## Tests
Run tests:

    docker-compose -f .\docker-compose.test.yml build
    docker-compose -f .\docker-compose.test.yml up