const { ApolloServer, AuthenticationError } = require("apollo-server-express");
const { ApolloGateway, RemoteGraphQLDataSource } = require("@apollo/gateway");
const express = require("express");
const expressJwt = require("express-jwt");
const app = express();

const startUpCheck = require("./startupCheck");

// A simple wrapper used by all services which need to be authenticated to set the token as a header
class AuthenticatedDataSource extends RemoteGraphQLDataSource {
  willSendRequest({ request, context }) {
    if (
      request.query !==
      "query __ApolloGetServiceDefinition__ { _service { sdl } }"
    ) {
      if (context.user) {
        //console.log("REQUEST", context.user);
        //placing a stringified user object in the request headers to the federated services
        // { username: string, id: string }
        request.http.headers.set(
          "user",
          context.user ? JSON.stringify(context.user) : null
        );
      } else {
        throw new AuthenticationError("you must be authenticated");
      }
    }
  }
}

//
startUpCheck(
  ["http://counter-service:4001", "http://authentication-service:4001", "http://user-service:4001"],
  function () {
    // Initialize an ApolloGateway instance and pass it an array of
    // your implementing service names and URLs
    const gateway = new ApolloGateway({
      serviceList: [
        {
          name: "counter",
          url: "http://counter-service:4001",
          authentication: true,
        },
        {
          name: "authentication",
          url: "http://authentication-service:4001",
          authentication: false,
        },
        {
          name: "user",
          url: "http://user-service:4001",
          authentication: true,
        },
        //{ name: 'user', url: 'http://user-service:4001' },
        //{ name: 'exercise', url: 'http://exercise-service:4001' },
        //{ name: 'communication', url: 'http://communication-service:4001' },
        // Define additional services here
      ],
      buildService({ url, authentication }) {
        if (authentication) {
          return new AuthenticatedDataSource({ url });
        } else {
          return new RemoteGraphQLDataSource({ url });
        }
      },
    });

    // Pass the ApolloGateway to the ApolloServer constructor
    const server = new ApolloServer({
      gateway,
      // Disable subscriptions (not currently supported with ApolloGateway)
      subscriptions: false,
      context: ({ req }) => {
        const user = req.user || null;
        console.log({user})
        return { user };
      },
    });

    //To enable Playground
    const path = "/graph";
    app.use(path, (req, res, next) => {
      // Allow GET (Playground)
      if (req.method === "GET") {
        console.log("Open Playground");
        return next();
      }
      // Authenticate other GraphQL queries
      next();
    });

    // Use expressJwt to validate JWT Tokens
    app.use(
      expressJwt({
        secret: process.env.JWT_SECRET,
        algorithms: ["HS256"],
        credentialsRequired: false,
      })
    );

    server.applyMiddleware({ app, path: path });

    app.listen({ port: process.env.PORT }, () => {
      console.log(
        `🚀 GraphQL Server ready at http://localhost:${process.env.PORT} at path ${server.graphqlPath}`
      );
    });
  }
);
