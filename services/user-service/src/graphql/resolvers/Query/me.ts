import { Context } from '../../../start-service';
import { IUser } from '../../../models/User';
import User from '../../../models/User';
import Mongoose from 'mongoose';

const meResolver = async (parent: void, args:void, context:Context) => {
    let usr = await User.findOne({id: context.user.id });
    if (usr) {
        return usr;
    } else if (context.user) {
        usr = await createUser(context.user);
        return usr;
    }
}

/**
 * Adds a new user to the database, if the requested user doesn't exist.
 * @param user 
 */
async function createUser(user:IUser):Promise<Mongoose.Document> {
    let usr = new User(user);
    await usr.save();
    return usr;
}

export default meResolver;