import { gql } from 'apollo-server';

import user from './user';
import user_m from './user_M';

const typeDefs:any = gql`
    ${user},
    ${user_m}
`

export default typeDefs;