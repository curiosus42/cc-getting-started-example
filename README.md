<!-- PROJECT SHIELDS -->
<!--
*** Markdown Guidelines
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)


<!-- PROJECT LOGO -->
<br />
<p align="center">
  <a href="https://gitlab.com/curiosus42/cc-getting-started-example">
  <p align="center"> 
    <img src="./documentation/images/cover_image.jpg" alt="Logo" height="120">
  <p/>
  </a>

<!-- PROJECT MENU -->
  <h2 align="center">Getting Started with DevOps Example Repo</h2>
  <p align="center"> This repository contains an example implementation with annotations and further explanations of an microservice backend with various development operations.
    <br />
    <a href="https://blog.mi.hdm-stuttgart.de/index.php/2020/09/29/getting-started-with-devops/"><strong>Explore the Blog Post »</strong></a>
    <br />
    <br />
    <a href="https://gitlab.com/-/ide/project/curiosus42/cc-getting-started-example/tree/master/-/documentation/Testing-Strategies.md/">Readme Testing Strategies</a>
    ·
    <a href="https://gitlab.com/-/ide/project/curiosus42/cc-getting-started-example/tree/master/-/documentation/Git-Workflow.md/">Readme Git Workflow</a>
    ·
    <a href="https://gitlab.com/-/ide/project/curiosus42/cc-getting-started-example/tree/master/-/documentation/GitLab-CICD.md/">Readme GitLab CI/CD</a>
  </p>
</p>



<!-- TABLE OF CONTENTS -->
## Table of Contents

- [Table of Contents](#table-of-contents)
- [About The Project](#about-the-project)
  - [Built With](#built-with)
- [Getting Started](#getting-started)
  - [Prerequisites](#prerequisites)
  - [Installation](#installation)
- [Usage](#usage)
- [License](#license)
- [Contact](#contact)
- [Acknowledgements](#acknowledgements)


<!-- ABOUT THE PROJECT -->
## About The Project

This project shall give a handy example of how to get started with the topic DevOps within a University project.

Of course, no one template will serve all projects since your needs may be different. So further reading information can not only be found in the blog post, but further more a list of commonly used resources that I find helpful are listed in the acknowledgements.

### Built With
This section lists any major frameworks that this project was built with. 
* [React](https://reactjs.org/)
* [NodeJS](https://nodejs.org/en/)
  
<!-- GETTING STARTED -->
## Getting Started

This is an example of how you may be setting up a test project locally.
To get a local copy up and running follow these simple steps.

<!-- PREREQUISITES -->
### Prerequisites

Things you need to use the software and how to install them.

* npm
```
npm install
```


<!-- INSTALLATION GUIDE -->
### Installation

1. Clone the repo
```
git clone https://github.com/your_username_/Project-Name.git
```
2. Install NPM packages
```
npm install
```
3. Install docker and docker-compose
```
https://docs.docker.com/get-docker/
```
1. Build docker container
```
docker-compose -f docker-compose.dev.yml build
```
5. Aggregate the output of each container
```
docker-compose -f docker-compose.dev.yml up
```


<!-- USAGE EXAMPLES -->
## Usage

Use this space to show useful examples of how a project can be used. Additional screenshots, code examples and demos work well in this space. You may also link to more resources.

_For more examples, please refer to the [Documentation](https://example.com)_


<!-- LICENSE -->
## License
Distributed under the MIT License. See `LICENSE` for more information.


<!-- CONTACT -->
## Contact
Project Link: [Getting Started with DevOps Repository](https://gitlab.mi.hdm-stuttgart.de/schule-4.0/cc-getting-started-example/)



<!-- ACKNOWLEDGEMENTS -->
## Acknowledgements

<!-- wp:heading {"level":3} -->
<h3>Architecture</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p><em>Getting started with your project</em><em><br></em>Software Development Life Cycle: A Guide to Phases and Models<br><a href="https://ncube.com/blog/software-development-life-cycle-guide">https://ncube.com/blog/software-development-life-cycle-guide</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><em>Why software architecture matters</em><em><br></em>Is High Quality Software Worth the Cost? - Martin Fowler<br><a href="https://martinfowler.com/articles/is-quality-worth-cost.html">https://martinfowler.com/articles/is-quality-worth-cost.html</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Testing Strategies in a Microservice Architecture - Toby Clemson<br><a href="https://martinfowler.com/articles/microservice-testing/">https://martinfowler.com/articles/microservice-testing/</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><em>Best resource for microservice related topics with examples</em><br>Microservice patterns - Chris Richardson<br><a href="https://microservices.io/patterns/">https://microservices.io/patterns/</a><em>The 12 factors that really matter when developing software</em><br><a href="https://12factor.net/">https://12factor.net/</a></p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3><strong>Git Workflow</strong></h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Git Workflow: <a href="https://www.atlassian.com/de/git/tutorials/comparing-workflows/gitflow-workflow" target="_blank" rel="noreferrer noopener">https://www.atlassian.com/de/git/tutorials/comparing-workflows/gitflow-workflow</a></p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3><strong>CI/CD Pipeline</strong></h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>GitLay CI/CD Pipeline General: <a href="https://docs.gitlab.com/ee/ci/pipelines/" target="_blank" rel="noreferrer noopener">https://www.atlassian.com/de/git/tutorials/comparing-workflows/gitflow-workflow</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Continuous Integration:</strong><br>Definition of Continuous Integration - Martin Fowler<br><a href="https://martinfowler.com/articles/continuousIntegration.html/">https://martinfowler.com/articles/continuousIntegration.html/</a><br><br><strong>Continuous Delivery:</strong><br>Continuous Delivery - Martin Fowler<br><a href="https://www.martinfowler.com/bliki/ContinuousDelivery.html">https://www.martinfowler.com/bliki/ContinuousDelivery.html</a><br>Architecture - Jez Humble <a href="https://continuousdelivery.com/implementing/architecture/">https://continuousdelivery.com/implementing/architecture/</a><br>Principles - Jez Humble <br>https://continuousdelivery.com/principles/</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Continuous Deployment:</strong><br>Continuous Deployment Blog - Timothy Fitz <a href="http://timothyfitz.com/2009/02/08/continuous-deployment/">http://timothyfitz.com/2009/02/08/continuous-deployment/</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>GitLab CI/CD:</strong><br>Rollbacks - GitLab Docs <a href="https://docs.gitlab.com/ee/ci/environments/#retrying-and-rolling-back">https://docs.gitlab.com/ee/ci/environments/#retrying-and-rolling-back</a><br>Incremental Rollouts - GitLab Docs <a href="https://docs.gitlab.com/ee/ci/environments/incremental_rollouts.html">https://docs.gitlab.com/ee/ci/environments/incremental_rollouts.html</a><br>CI Runner - GitLab Docs <br><a href="https://docs.gitlab.com/ee/ci/runners/README.html">https://docs.gitlab.com/ee/ci/runners/README.html</a><br>Pipeline Linting - GitLab Docs<br><a href="https://docs.gitlab.com/ee/ci/lint.html">https://docs.gitlab.com/ee/ci/lint.html</a><br>CI/CD Job Logging - GitLab Docs<br><a href="https://docs.gitlab.com/ee/administration/job_logs.html">https://docs.gitlab.com/ee/administration/job_logs.html</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Containerization and virtualization:</strong><br>Docker: <a href="https://www.docker.com/">https://www.docker.com/</a><br>Docker integration - GitLab Docs<br><a href="https://docs.gitlab.com/ee/ci/docker/README.html">https://docs.gitlab.com/ee/ci/docker/README.html</a><br>Vagrant virtualization - Vagrant<br><a href="https://www.vagrantup.com/">https://www.vagrantup.com/</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Runner installation:</strong><br>AWS EC2 Instance <br><a href="https://aws.amazon.com/de/getting-started/">https://aws.amazon.com/de/getting-started/</a><br>Runner installation on RaspberryPi - Sebastian Martens<br><a href="https://blog.sebastian-martens.de/technology/install-gitlab-runner-on-raspberry-pi/">https://blog.sebastian-martens.de/technology/install-gitlab-runner-on-raspberry-pi/</a><br>GitLab Runner <br><a href="https://docs.gitlab.com/runner/">https://docs.gitlab.com/runner/</a></p>
<!-- /wp:paragraph -->

<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[contributors-url]: https://gitlab.com/curiosus42/cc-getting-started-example/graphs/contributors
