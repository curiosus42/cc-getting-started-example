import queries from './Query';
import mutations from './Mutations';
import customs from './Custom';

const resolvers:any = { 
    Query: {
        ...queries
    }, 
    Mutation: {
        ...mutations
    },
    ...customs
};

export default resolvers;