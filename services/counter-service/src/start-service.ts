import { ApolloServer } from 'apollo-server';
import { buildFederatedSchema } from '@apollo/federation';
import typeDefs from './graphql/typeDefs';
import resolvers from './graphql/resolvers';

import { Pool } from 'pg';

async function connectDb(counter:number):Promise<Pool> {
    if (counter <= 5) {
        let pool = new Pool();
        try {
            await pool.query("SELECT NOW()");
        } catch (err) {
            if (err.code == "ENOTFOUND") {
                console.log(`Couldn't establish DB connection after ${counter}/5 retries.`)
                return await new Promise((res, rej) => {
                    setTimeout(async () => {
                        res(await connectDb(counter + 1))
                    }, 2000)
                })
            } else {
                throw err;
            }
        }
        console.log(`Successfully established DB connection after ${counter} retries.`)
        return pool;
    } else {
        console.error("Couldn't establish database connection after 5 retries.")
        process.exit(-1);
    }
}

export interface Context {
    user: {
        id: string;
    },
    pool: Pool;
}

const start = async () =>  {

    let pool = await connectDb(0);

    console.log("Resolvers:", resolvers)

    const server = new ApolloServer({
      schema: buildFederatedSchema({ typeDefs, resolvers }),
      context: ({ req, res }):Context | void => {
        //Extracting the current user from the request header
        const user = req.get("user");
        if (user) {
            try {
                return { 
                    user: JSON.parse(user),
                    pool: pool
                };
            } catch(err) {
                console.error("Error during JSON parsing user:", err)
            }
        }
        // user: { userId:string }  
      }
    });
    
    server.listen({port: process.env.PORT}).then(({ url }) => {
        console.log(`🚀 Server ready at ${url}`);
    });

}
start();