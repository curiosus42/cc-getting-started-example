# graphql-gateway

## Key Features:
* Schema Federation
* Validates the JWTs in the requests
* Can check whether all necessary microservices are up and running correctly.

## Schema federation
This application combines the graph schemas of the microservices to a single large graph through federation. It is the only public available application from the backend (except for the server which serves the client app).

![visualizazion of the federated graph](data-graph.jpg)
*Visualization of the federated data graph*

You can have a look at the Schema on your own by visitting http://localhost:4001/graph

## JWT validation
Our authorization mechanism is based on a JSON Web Token (JWT). Your can read more about that in the `\services\authentication-service`.

The gateway validates the JWT (access token) from the incoming client requests and decodes them.

    { //Decoded JWT
        "username": string, 
        "id": string
    }

This JSON Object is then stringified and included in all outgoing requests from the gateway to the services. If the JWT was inavlid, the request is rejected.

There is also an option, whether a service can only accept authorized requests or is public available. For example, the authentiation-service needs to be available for everyone and is therefore not checked for valid JWT:

    {
        name: "authentication",
        url: "http://authentication-service:4001",
        authentication: false //JWT is not checked
    }

## Startup checks
The gateway makes sure that all services are ready before accepting any requests. This is done by calling a special endpoint on the Apollo Server running in the services. It is implemented in `\src\startupCheck.js`.

## Further reading
Read more about Federation: https://www.apollographql.com/docs/apollo-server/federation/introduction/

*Note:* The technique is relatively new and lacks some features compared to the apollo graphql default library. Some of these limitations are discussed in our [blog post](https://blog.mi.hdm-stuttgart.de/index.php/2020/08/21/getting-started-with-devops/).